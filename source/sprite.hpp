#pragma once
#include <grrlib.h>
#include "vector2.hpp"
#include "MonoBehaviour.hpp"

namespace tu {
	class Sprite: public MonoBehaviour {
	private:

	public:
		enum class DrawingMode {
			kSubRect, kFullTexture, kTileQuad, count, first = kSubRect, last = kTileQuad
		};
		GRRLIB_texImg* texture;
		Vector2f uv;
		Vector2f size;
		unsigned int tint;
		//uv arent real uv since they are not in the 0 ... 1 range, so just use pixel offsets,
		//but having two parts of a sprite named x and y made no sense and they are still texture coords, so uv works
		Sprite(): texture(nullptr), uv(Vector2f(0.0f, 0.0f)), tint(0xffffffff) {};
		Sprite(Sprite* s): texture(s->texture), uv(s->uv), tint(s->tint) {};
		void adjustColliderBounds();
		virtual void draw();
	};
}
