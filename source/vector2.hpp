#pragma once
namespace tu {
	template <class T>
	struct Vector2 {
	public:
		T x, y;

		Vector2(T x_, T y_): x(x_), y(y_) {};
		Vector2(): x(0), y(0) {};
	};
	template <class T>
	bool operator ==(const Vector2<T>& left, const Vector2<T>& right) {
		return(left.x == right.x) && (left.y == right.y);
	}
	template <class T>
	bool operator !=(const Vector2<T>& left, const Vector2<T>& right) {
		return (left.x != right.x) && (left.y != right.y);
	}
	template <class T>
	Vector2<T> operator /=(Vector2<T>& left, const Vector2<T>& right) {
		left.x /= right.x;
		left.y /= right.y;
		return left;
	}
	template <class T>
	Vector2<T> operator -(const Vector2<T>& r) {
		return Vector2<T>(-r.x, -r.y);
	}
	template <class T>
	Vector2<T> operator -(const Vector2<T>& left, const Vector2<T>& right) {
		return Vector2<T>(left.x - right.x, left.y - right.y);
	}
	template <class T>
	Vector2<T> operator -(const Vector2<T>& left, const T right) {
		return Vector2<T>(left.x - right, left.y - right);
	}
	template <class T>
	Vector2<T> operator -=(Vector2<T>& left, const T right) {
		left.x -= right;
		left.y -= right;
		return left;
	}
	template <class T>
	Vector2<T> operator -=(Vector2<T>& left, const Vector2<T>& right) {
		left.x -= right.x;
		left.y -= right.y;
		return left;
	}

	template <class T>
	Vector2<T> operator +(const Vector2<T>& left, const Vector2<T>& right) {
		return Vector2<T>(left.x + right.x, left.y + right.y);
	}
	template <class T>
	Vector2<T> operator +(const Vector2<T>& left, const T right) {
		return Vector2<T>(left.x + right, left.y + right);
	}
	template <class T>
	Vector2<T> operator +=(Vector2<T>& left, const Vector2<T>& right) {
		left.x += right.x;
		left.y += right.y;
		return left;
	}
	template <class T>
	Vector2<T> operator +=(Vector2<T>& left, const T right) {
		left.x += right;
		left.y += right;
		return left;
	}

	template <class T>
	Vector2<T> operator *(const Vector2<T>& left, const Vector2<T>& right) {
		return Vector2<T>(left.x * right.x, left.y * right.y);
	}
	template <class T>
	Vector2<T> operator *(Vector2<T>& left, const T right) {
		return Vector2<T>(left.x * right, left.y * right);
	}
	template <class T>
	Vector2<T> operator *=(Vector2<T>& left, const Vector2<T>& right) {
		left.x *= right.x;
		left.y *= right.y;
		return left;
	}
	template <class T>
	Vector2<T> operator *=(Vector2<T>& left, T right) {
		left.x *= right;
		left.y *= right;
		return left;
	}
	template <class T>
	Vector2<T> operator /(const Vector2<T>& left, const Vector2<T>& right) {
		return Vector2<T>(left.x / right.x, left.y / right.y);
	}
	template <class T>
	Vector2<T> operator /(const Vector2<T>& left, T right) {
		return Vector2<T>(left.x * right, left.y * right);
	}
	template <class T>
	Vector2<T> operator /=(Vector2<T>& left, const T right) {
		left.x /= right;
		left.y /= right;
		return left;
	}
	using Vector2f = Vector2<float>;
	using Vector2i = Vector2<int>;
	using CiwSVec2 = Vector2<short>;
}
