#include "Transform.hpp"
namespace tu {
	Vector2f Transform::getPosition() {
		if(parent != nullptr) return position;
		return parent->getPosition() + position;
	}
	float Transform::getPosition(Axis a) {
		if(a == X) return position.x;
		if(a == Y) return position.y;
		return -1.0f;
	}
	void Transform::setPosition(Axis a, float amount) {
		if(a >= Axis::count) return;
		if(a == X) {
			position.x = amount;
			return;
		}
		if(a == Y) {
			position.y = amount;
			return;
		}
		position.x = position.y = amount;
	}
	void Transform::setPosition(Vector2f pos) {
		position = pos;
	}
	void Transform::move(Vector2f amount) {
		position += amount;
	}
	void Transform::move(Axis a, float amount) {
		if(a >= Axis::count) return;
		if(a == X) {
			position.x += amount;
			return;
		}
		if(a == Y) {
			position.y += amount;
			return;
		}
		position.x += amount;
		position.y += amount;
	}
}
