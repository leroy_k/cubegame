#pragma once
#include "vector2.hpp"
#include "MonoBehaviour.hpp"
namespace tu {
	class Transform: public MonoBehaviour {
	public:
		Transform(): position(Vector2f(0.0f, 0.0f)), scale(Vector2f(1.0f, 1.0f)), rotation(0.0f), parent(nullptr) {};
		virtual ~Transform(){};

		Vector2f position, scale;
		float rotation;
		Transform* parent;
		enum Axis {
			X, Y, Both, count, first = X, last = Both
		};

		Vector2f getPosition();
		float getPosition(Axis a);
		void setPosition(Axis a, float amount);
		void setPosition(Vector2f pos);
		void move(Vector2f amount);
		void move(Axis a, float amount);
	};
}
