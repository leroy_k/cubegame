#pragma once
#include "types.hpp"
#include "Sprite.hpp"
#include "ButtonList.hpp"
#include "MonoBehaviour.hpp"

namespace tu {
	class View: public MonoBehaviour {
	protected:
		//TODO: should add some platform detects to enable/disable transitioning
		Sprite m_Background;
		bool m_TransitionDirection,
			m_TransitionInProgress,
			m_TransitionCompleted;
		float m_TransitionStartPosition,
			m_TransitionEndPosition;
		Vector2f m_TransitionPosition = Vector2f(0.0f, 0.0f);
		float m_TransitionVelocity,
			m_TransitionPositionIncrement;
		View* m_TransitionBuddy = nullptr;
		ButtonList m_Buttons;
		uint8 m_UniqueId;
		static Vector2f size;
	public:
		static const bool kTransitionDirectionLeft = false;
		static const bool kTransitionDirectionRight = true;

		View();
		virtual ~View();

		virtual void draw();
		virtual void update();
		virtual void updateTransition();
		virtual uint8 startTransition(bool direction, View* endView);
		virtual bool transitionHasCompleted();
		virtual bool isTransitioning();
		uint8 getUniqueId();
	};
}
