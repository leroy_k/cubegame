#include <iostream>
#include <memory>
#include "sprite.hpp"
#include "BoxCollider.hpp"

namespace tu {
	void Sprite::draw() {
		if(!texture) return;
		Vector2f pos = transform->getPosition();
		GRRLIB_DrawPart(pos.x, pos.y,	uv.x, uv.y,
				((size.x == 0.0f) ? texture->w : size.x), ((size.y == 0.0f) ? texture->h : size.y),
				texture, transform->rotation,
				transform->scale.x, transform->scale.y, tint);
	}
	void Sprite::adjustColliderBounds() {
		std::shared_ptr<BoxCollider> collider = gameObject->getComponent<BoxCollider>();
		if(collider == nullptr) {
			std::cout << "[Error] No BoxCollider is attached to this Sprite" << std::endl;
			return;
		};
		collider->setSize(Vector2f(texture->w >> 1, texture->h >> 1));
	}
}
