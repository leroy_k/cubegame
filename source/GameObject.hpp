#pragma once
#include <vector>
#include <type_traits>
#include <memory>

namespace tu {
	class MonoBehaviour;

	class GameObject {
	private:
		std::vector<std::shared_ptr<MonoBehaviour>> m_Components;
		void update();
		void start();
		void onDestroy();
	public:

		template<class T>
		std::shared_ptr<T> addComponent() {
			// static_assert(std::is_base_of<MonoBehaviour, T>::value, "Components must be derived from MonoBehaviour");
			auto t = std::make_shared<T>(new T());
			auto ret = t;
			m_Components.push_back(t);
			// ret->gameObject = this;
			// ret->transform = transform;
			return ret;
		}
		template<class T>
		std::shared_ptr<T> getComponent() {
			// static_assert(std::is_base_of<MonoBehaviour, T>::value, "Components must be derived from MonoBehaviour");
			for(auto behaviourPtr : m_Components) {
				std::shared_ptr<T> requestedComponent = std::dynamic_pointer_cast<T>(behaviourPtr);
				if(requestedComponent != nullptr) return requestedComponent;
			}
			return std::shared_ptr<T>(nullptr);
		}
	};
}
