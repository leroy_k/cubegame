//
//  math.hpp
//  Hotplug
//
//  Created by Leroy Ketelaars on 03/10/14.
//  Copyright (c) 2014 Leroy Ketelaars. All rights reserved.
//

#pragma once
#include <cmath>
#include "types.hpp"
#include "vector2.hpp"

namespace tu {
	namespace math {
		const auto kPi = 3.141592653589793;
		const auto kTau = 6.283185307179586;
		template<class T>
		T distance(const Vector2<T>& a, const Vector2<T>& b){
			return std::sqrt(((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y)));
		}
		template<class T>
		T magnitude(const Vector2<T>& vec) {
			return sqrt(vec.x * vec.x + vec.y * vec.y);
		}

		template<class T>
		T dot(const Vector2<T>& a, const Vector2<T>& b) {
			return (a.x * b.x) + (a.y * b.y);
		}

		template<class T>
		T radToDeg(const T& rad) {
			return rad * 180 / kPi;
		}
		template<class T>
		T degToRad(const T& degrees) {
			return degrees * kPi / 180;
		}
		template<class T>
		void normalize(Vector2<T>& vec) {
			float magnitude = sqrtf((vec.x * vec.x) + (vec.y * vec.y));
			vec.x /= magnitude;
			vec.y /= magnitude;
		}
		template<class T>
		bool arePerpendicular(const Vector2<T>& a, const Vector2<T>& b) {
			return (dot(a, b) == 0);
		}

		template<class T>
		Vector2<T> normal(const Vector2<T>& a, const Vector2<T>& b) {
			return Vector2<T>(-(b.y - a.y), (b.x - a.x));
		}

		//rotate vector around an origin.. found this on the internets, no clue if works or useful :P
		//it allows you to rotate a point around another point by specifying an angle (I am assuming i works in degrees..)
		template<class T>
		Vector2<T> rotateVecAroundPoint(const Vector2<T>& vec, const Vector2<T>& origin, double angle) {
			return Vector2<T>(
				((vec.x - origin.x) * cos(angle)) - ((origin.y - vec.y) * sin(angle)) + origin.x,
				((origin.y - vec.y) * cos(angle)) - ((vec.x - origin.x) * sin(angle)) + origin.y
			);
		}

		template<class T>
		float angleBetweenPointsRadians(const Vector2<T>& a, const Vector2<T> b) {
			return atan2f(a.y - b.y, a.x - b.x);
		}
		template<class T>
		float angleBetweenPointsDegrees(const Vector2<T>& a, const Vector2<T> b) {
			return atan2f(a.y - b.y, a.x - b.x) * 180 / kPi;
		}

		Vector2f vectorFromAngle(const float& angleAsDegrees);

		template<class T>
		bool linesIntersect(//line A
							Vector2<T> a1, Vector2<T> a2,
							//line B
							Vector2<T> b1, Vector2<T> b2,
							Vector2<T>& intersectionPoint) {
			if((a1.x == a2.x && a1.y == a2.y) || (b1.x == b2.x && b1.y == b2.y)) {
				return false; //lines can't intersect
			}

			//translate the coord system so point A is on the origin
			a2.x -= a1.x; a2.y -= a1.y;
			b1.x -= a1.x; b1.y -= a1.y;
			b2.x -= a1.x; b2.y -= a1.y;

			double lengthA, theCos, theSin, newX, positionA;
			// find the length of line A (A1.x, A1.y are now at 0,0)
			lengthA = magnitude(a2);

			//rotate the coord system so A2 is on the positive axis (this way -> )
			theCos = a2.x / lengthA;
			theSin = a2.y / lengthA;
			newX = b1.x * theCos + b1.y * theSin;
			b1.y = b1.y * theCos - b1.x * theSin;
			b1.x = newX;
			newX = b2.x * theCos + b2.y * theSin;
			b2.y = b2.y * theCos - b2.x * theSin;
			b2.x = newX;

			//fail if the lines are parralel to eachother
			if(b1.y == b2.y) return false;

			//find position of intersection point along line A
			positionA = b2.x + (b1.x - b2.x) * b2.y / (b2.y - b1.y);

			//fail if line B doesn't cross line A
			if(positionA < 0 || positionA > lengthA) {
				return false;
			}

			//apply the position to line A in the original coord system (not rotated or translated)
			intersectionPoint = Vector2<T>(
				a1.x + positionA * theCos,
				a1.y + positionA * theSin
			);

			return true;
		}
	}
}
