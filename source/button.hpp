#pragma once
#include <functional>
#include <string>
namespace tu {
	class Button {
	public:
		std::string caption;
		std::function<void (void)> callback;
		Button(std::string text, std::function<void (void)> call) {
			caption = text;
			callback = call;
		}

		Button(std::function<void (void)> call) {
			caption = "button";
			callback = call;
		}
		Button() = default;
		virtual void fire() {
			if(callback) callback();
		}
	};
}
