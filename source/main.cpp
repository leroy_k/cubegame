/*===========================================
GRRLIB (GX Version)
- Template Code -

Minimum Code To Use GRRLIB
============================================*/
#include <grrlib.h>
#include <functional>
#include <stdlib.h>
#include <ogc/pad.h>
#include "buttonlist.hpp"
#include "button.hpp"
#include "bebas_ttf.h"
#include "background_png.h"
#include "CircularBattleTex_png.h"
#include "sprite.hpp"
#include "BoxCollider.hpp"
#include "GameObject.hpp"
#include "Transform.hpp"

using namespace tu;

class testclass {
	public:
	void test(void) {
		int a = 9;
		a *= M_PI;
	}
};
void okay(void) {
	printf("cool");
}

/**
	TODO: whip up an on-screen console window using GRRLIB bitmap fonts
	TODO: a gameobject manager
	TODO: more UI (text, textbox, virtual keyboard)
	TODO: a resource manager
	TODO: an input manager
	TODO: more types of colliders
	TODO: inplement some math::Lerp
	TODO: memory card access
	TODO: network access
*/


int main(int argc, char **argv) {
	// Initialise the Graphics & Video subsystem
	GRRLIB_Init();

	// Initialise the Wiimotes
	PAD_Init();

	GRRLIB_ttfFont* bebasFont = GRRLIB_LoadTTF(bebas_ttf, bebas_ttf_size);
	GRRLIB_texImg* bgTexture = GRRLIB_LoadTexture(background_png);
	GRRLIB_texImg* battleTexture = GRRLIB_LoadTexture(CircularBattleTex_png);
	tu::ButtonList buttons;
	testclass a;

	GameObject go;
	auto sprite = go.addComponent<Sprite>();
	sprite->texture = battleTexture;
	sprite->uv = Vector2f(0.0f, 0.0f);
	sprite->size = Vector2f(100.0f, 100.0f);
	sprite->adjustColliderBounds();

	buttons.add("test", std::bind(&testclass::test, &a));
	buttons.add("okay", std::bind(&okay));
	buttons.configureLayout(100.0f, 10.0f, tu::ButtonList::LayoutOrientation::Vertical);
	buttons.configureStyling(4.0f, 10.0f, 140.0f, 100.0f);
	buttons.configureText(bebasFont, 43, tu::ButtonList::DefaultColors::Magenta, tu::ButtonList::DefaultColors::White, tu::ButtonList::DefaultColors::Black);

	// tu::Sprite testSprite(battleTexture, 0.0f, 0.0f, 100.0f, 100.0f);
	int mode = 0;
	// Loop forever
	while(1) {

		PAD_ScanPads();  // Scan the Wiimotes

		// If [HOME] was pressed on the first Wiimote, break out of the loop
		if (PAD_ButtonsDown(0) & PAD_BUTTON_START)  break;

		if(PAD_ButtonsDown(0) & PAD_BUTTON_A) {
			++mode;
			if(mode > 2) mode = 0;
		}
		switch(mode) {
			case 0: {
				if(PAD_ButtonsDown(0) & PAD_BUTTON_RIGHT) sprite->size.x += 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_LEFT) sprite->size.x -= 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_UP) sprite->size.y += 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_DOWN) sprite->size.y -= 2.0f;
				break;
			}
			case 1: {
				if(PAD_ButtonsDown(0) & PAD_BUTTON_RIGHT) sprite->uv.x += 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_LEFT) sprite->uv.x -= 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_UP) sprite->uv.y += 2.0f;
				if(PAD_ButtonsDown(0) & PAD_BUTTON_DOWN) sprite->uv.y -= 2.0f;
				break;
			}
			case 2: {
				if(PAD_ButtonsDown(0) & PAD_BUTTON_RIGHT) sprite->transform->move(Transform::Axis::X, 2.0f);
				if(PAD_ButtonsDown(0) & PAD_BUTTON_LEFT) sprite->transform->move(Transform::Axis::X, -2.0f);
				if(PAD_ButtonsDown(0) & PAD_BUTTON_UP) sprite->transform->move(Transform::Axis::Y, 2.0f);
				if(PAD_ButtonsDown(0) & PAD_BUTTON_DOWN) sprite->transform->move(Transform::Axis::Y, -2.0f);
				break;
			}
			default:break;
		}

		if(PAD_ButtonsDown(0) & PAD_TRIGGER_Z) buttons.selectionWrapsAround = !buttons.selectionWrapsAround; //uttons.layoutOrientation = !buttons.layoutOrientation;
 		GRRLIB_FillScreen(0x000000ff);    // Clear the screen
		GRRLIB_DrawImg(0, 0, bgTexture, 0, (1.0/512)*640, 1.42f, 0xffffffff);
		buttons.draw();
		sprite->draw();
		
		// GRRLIB_Printf(5, 25, bebasFont, 0xffffffff, 1, "This will turn into a console window some day");

		// ---------------------------------------------------------------------
		// Place your drawing code here
		// ---------------------------------------------------------------------

		GRRLIB_Render();  // Render the frame buffer to the TV
	}
	GRRLIB_FreeTTF(bebasFont);
	GRRLIB_Exit(); // Be a good boy, clear the memory allocated by GRRLIB

	exit(0);  // Use exit() to exit a program, do not use 'return' from main()
}
