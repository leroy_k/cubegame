#pragma once
#include "vector2.hpp"
#include "types.hpp"
#include "Transform.hpp"
namespace tu {
	class Drawable {
	protected:
		Transform m_Transform;
	public:
		Drawable() {};
		virtual ~Drawable() {};

		virtual void update();
		virtual void draw() = 0;
	};
}
