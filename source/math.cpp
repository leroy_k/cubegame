//
//  math.cpp
//  Hotplug
//
//  Created by Leroy Ketelaars on 03/10/14.
//  Copyright (c) 2014 Leroy Ketelaars. All rights reserved.
//

#include "math.hpp"

namespace tu {
	namespace math {
		Vector2f vectorFromAngle(const float& angleAsDegrees) {
			float angleAsRadians = degToRad(angleAsDegrees);
			return Vector2f(sin(angleAsRadians), cos(angleAsRadians));
		}
	}
}
