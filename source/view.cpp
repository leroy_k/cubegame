#include "view.hpp"
#include "time.hpp"
#include "Transform.hpp"
namespace tu {
	View::View() {
		m_TransitionDirection = kTransitionDirectionLeft;
		m_TransitionInProgress = false;
		m_TransitionVelocity = 40.0f;
		m_TransitionCompleted = false;
		m_TransitionBuddy = nullptr;
		m_Background.transform->parent = transform;
	}
	View::~View() {

	}

	void View::draw() {
		m_Background.draw();
		m_Buttons.draw();
		if(m_TransitionInProgress)
		if(m_TransitionBuddy != nullptr)
		m_TransitionBuddy->draw();
	}
	void View::updateTransition() {
		if(!m_TransitionInProgress) return;
		auto position = transform->getPosition();
		float diff = m_TransitionEndPosition - position.x;
		if(diff < 0) diff = -diff;
		if(diff < 0.001f) {
			m_TransitionInProgress = false;
			m_TransitionCompleted = true;
			m_TransitionBuddy = nullptr;
			return;
		}
		//TODO: add Time.deltaTime here. problem is, it needs to be a float, but it is currently an int (in the original game)
		transform->move(Transform::Axis::X, ((m_TransitionDirection == kTransitionDirectionRight) ? m_TransitionVelocity : -m_TransitionVelocity) * 1.0f);
	}
	uint8 View::startTransition(bool direction, View* endView) {
		m_TransitionBuddy = endView;
		m_TransitionDirection = direction;
		if(m_TransitionBuddy != nullptr) {
			switch(m_TransitionDirection) {
				case kTransitionDirectionLeft:
					m_TransitionBuddy->transform->setPosition(Vector2f(-View::size.x, 0.0f));
				break;
				case kTransitionDirectionRight:
					m_TransitionBuddy->transform->setPosition(Vector2f(View::size.x, 0.0f));
				break;
				default: break;
			}
		}
		m_TransitionCompleted = false;
		m_TransitionInProgress = true;
		return ((m_TransitionBuddy != nullptr) ? m_TransitionBuddy->getUniqueId() : 0);
	}
	bool View::isTransitioning() {
		return m_TransitionInProgress;
	}
	uint8 View::getUniqueId() {
		return m_UniqueId;
	}
}
