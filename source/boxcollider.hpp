#pragma once
#include "types.hpp"
#include "vector2.hpp"
#include "Bounds.hpp"
#include "Transform.hpp"
#include "MonoBehaviour.hpp"
namespace tu {
	class BoxCollider: public MonoBehaviour {
	public:
		Vector2f size;

		BoxCollider(): size(0.0f, 0.0f) {};

		bool intersects(const BoxCollider& b) const;
		bool contains(const Vector2f& v) const;
		Bounds getBounds() const;
		void setSizeFromBounds(const Bounds& b);
		void setSize(const Vector2f& s);
	};
}
