#include <grrlib.h>
#include "buttonlist.hpp"
namespace tu {
	ButtonList::ButtonList() {
		setSelection(0);
	}
	void ButtonList::add(std::string caption, std::function<void (void)> function) {
		m_Buttons.push_back(new Button(caption, function));
	}
	void ButtonList::setSelection(u8 selectionIndex) {
		if(selectionIndex < m_Buttons.size()) selection = m_SelectionIndex = selectionIndex;
	}
	void ButtonList::selectionNext() {
		selection = ++m_SelectionIndex;

		if(selectionWrapsAround) {
			if(m_SelectionIndex > (m_Buttons.size()-1)) selection = m_SelectionIndex = 0;
			return;
		}
		if(m_SelectionIndex > (m_Buttons.size()-1)) selection = m_SelectionIndex = (m_Buttons.size()-1);
	}
	void ButtonList::selectionPrevious() {
		selection = --m_SelectionIndex;

		if(selectionWrapsAround) {
			if(m_SelectionIndex > (m_Buttons.size()-1)) selection = m_SelectionIndex = (m_Buttons.size()-1);
			return;
		}
		if(m_SelectionIndex > (m_Buttons.size()-1)) selection = m_SelectionIndex = 0;
	}
	void ButtonList::draw() {
		float calculatedposition = (layoutOrientation ? x : y);
		int selectedIndex = 0;
		for(Button* button : m_Buttons) {
			if(button == nullptr) {++selectedIndex; continue;};
			if(layoutOrientation == LayoutOrientation::Horizontal) {
				if(m_SelectionIndex == selectedIndex) {
					GRRLIB_Rectangle(calculatedposition, y, elementWidth, elementHeight, activeColor, true);
				} else {
					GRRLIB_Rectangle(calculatedposition, y, elementWidth, elementHeight, idleColor, true);
					GRRLIB_Rectangle(calculatedposition + borderWidth, y + borderWidth, elementWidth - (borderWidth*2), elementHeight - (borderWidth * 2), backgroundColor, true);
				}
				if(font != nullptr) GRRLIB_PrintfTTF(calculatedposition + elementCaptionOffsetX, y + elementCaptionOffsetY, font, button->caption.c_str(), fontSize, idleColor);
				calculatedposition += (padding + elementWidth);
			} else {
				if(m_SelectionIndex == selectedIndex) {
					GRRLIB_Rectangle(x, calculatedposition, elementWidth, elementHeight, activeColor, true);
				} else {
					GRRLIB_Rectangle(x, calculatedposition, elementWidth, elementHeight, idleColor, true);
					GRRLIB_Rectangle(x + borderWidth, calculatedposition + borderWidth, elementWidth - (borderWidth*2), elementHeight - (borderWidth * 2), backgroundColor, true);
				}
				if(font != nullptr) GRRLIB_PrintfTTF(x + elementCaptionOffsetX, calculatedposition + elementCaptionOffsetY, font, button->caption.c_str(), fontSize, idleColor);
				calculatedposition += (padding + elementHeight);
			}
			++selectedIndex;

		}

		// if(font != nullptr) GRRLIB_PrintfTTF(100.0f, 100.0f, font, m_DebugBuff, fontSize, idleColor);

	}
	void ButtonList::configureLayout(float xpos, float ypos, bool orientation) {
		x = xpos; y = ypos;
		layoutOrientation = orientation;
	}
	void ButtonList::configureStyling(float borderwidth, float buttonpadding, float buttonWidth, float buttonHeight) {
		elementWidth = buttonWidth;
		elementHeight = buttonHeight;
		borderWidth = borderwidth;
		padding = buttonpadding;
	}
	void ButtonList::configureText(GRRLIB_ttfFont* newfont, u32 fontsize, u32 activecolor, u32 normalcolor, u32 backgroundcolor) {
		font = newfont;
		fontSize = fontsize;
		activeColor = activecolor;
		idleColor = normalcolor;
		backgroundColor = backgroundcolor;
		elementCaptionOffsetY = (elementHeight / 2) - (fontSize / 2);
		elementCaptionOffsetX = (borderWidth * 3);
	}
	void ButtonList::callSelectedButton() {
		m_Buttons[ m_SelectionIndex ]->fire();
	}
}
