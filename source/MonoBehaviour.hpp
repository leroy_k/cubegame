#pragma once
#include "GameObject.hpp"

namespace tu {
	class Transform;
	class MonoBehaviour {
	public:
		GameObject* gameObject = nullptr;
		Transform* transform = nullptr;
		virtual void awake(){};
		virtual void start(){};
		virtual void onDestroy(){};
		virtual void update(){};
		virtual void draw(){};
	};
}
