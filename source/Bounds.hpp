#include "vector2.hpp"

namespace tu {
	class Bounds {
	public:
		Vector2f min, max;

		Bounds(){};
		Bounds(const Vector2f& min_, const Vector2f& max_): min(min_), max(max_) {

		}
	};
}
