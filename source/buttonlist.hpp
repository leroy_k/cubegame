#pragma once
#include <vector>
#include "button.hpp"
namespace tu {
	class ButtonList {
	private:
	std::vector<Button*> m_Buttons;
	u8 m_SelectionIndex;
	// char m_DebugBuff[100];
	public:
		struct LayoutOrientation {
			enum {
				Horizontal = true,
				Vertical = false,
			};
		};
		struct DefaultColors {
			enum {
				Black = 0x000000ff,
				White = 0xffffffff,
				Silver =  0xc0c0c0ff,
				Cyan =  0x00ffffff,
				Magenta = 0xff00ffff,
				Yellow = 0xffff00ff,
				Red = 0xff0000ff,
				Green = 0x00ff00ff,
				Blue = 0x0000ffff,
				Gray = 0x808080ff
			};
		};
		u8 selection;
		float x, y;
		float elementWidth, elementHeight;
		float padding = 0.0f;
		bool layoutOrientation = LayoutOrientation::Vertical;
		bool selectionWrapsAround = true;
		float borderWidth = 4;
		GRRLIB_ttfFont* font = nullptr;
		float elementCaptionOffsetX, elementCaptionOffsetY;
		u32 fontSize = 20;
		u32 activeColor = DefaultColors::Red, idleColor = DefaultColors::White, backgroundColor = DefaultColors::Black;

		ButtonList();

		void add(std::string caption, std::function<void (void)> function);
		void setSelection(u8 index);
		void selectionNext();
		void selectionPrevious();
		void draw();
		void configureLayout(float x, float y, bool orientation);
		void configureStyling(float borderwidth, float padding, float buttonWidth, float buttonHeight);
		void configureText(GRRLIB_ttfFont* font, u32 fontsize, u32 activecolor, u32 normalcolor, u32 backgroundcolor);
		void callSelectedButton();
	};

}
