#include "BoxCollider.hpp"
namespace tu {
	bool BoxCollider::contains(const Vector2f& v) const {
		Bounds b = getBounds();
		if(v.x < b.min.x) return false;
		if(v.x > b.max.x) return false;
		if(v.y < b.min.y) return false;
		if(v.y > b.max.y) return false;
		return true;
	}
	bool BoxCollider::intersects(const BoxCollider& b) const {
		Bounds myBounds = getBounds(),
		theirBounds = b.getBounds();
		if(theirBounds.min.x > myBounds.max.x) return false;
		if(theirBounds.max.x < myBounds.min.x) return false;
		if(theirBounds.min.y > myBounds.max.y) return false;
		if(theirBounds.max.y < myBounds.max.y) return false;
		return true;
	}
	Bounds BoxCollider::getBounds() const {
		Vector2f position = transform->getPosition();
		return Bounds(position - (size * transform->scale), position + (size * transform->scale));
	}
	void BoxCollider::setSizeFromBounds(const Bounds& b) {
		size = (b.max - b.min) / 2.0f;
	}
	void BoxCollider::setSize(const Vector2f& s) {
		size = s;
	}
}
